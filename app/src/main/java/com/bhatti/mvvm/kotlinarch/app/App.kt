package com.bhatti.mvvm.kotlinarch.app

import android.app.Activity
import android.app.Application
import com.bhatti.mvvm.kotlinarch.di.base.AppInjector
import com.bhatti.mvvm.kotlinarch.utils.extensions.FinjaPreferences
import com.bhatti.mvvm.kotlinarch.utils.locale.LanguageUtil
import com.bhatti.mvvm.kotlinarch.utils.locale.LocaleHelper
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasActivityInjector
import javax.inject.Inject


/**
 * Created by ahsan on 12,November,2019
 */

class App : Application(), HasActivityInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Activity>

    override fun onCreate() {
        super.onCreate()
        AppInjector.init(this)
        FinjaPreferences.init(this)
    }

    override fun activityInjector(): AndroidInjector<Activity> {
        return dispatchingAndroidInjector
    }
}