package com.bhatti.mvvm.kotlinarch.di.base

/**
 * Created by ahsan on 04,December,2019
 */

/**
 * Marks an activity / fragment injectable.
 */
interface Injectable
