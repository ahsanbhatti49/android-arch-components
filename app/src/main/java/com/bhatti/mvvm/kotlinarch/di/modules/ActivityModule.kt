package com.bhatti.mvvm.kotlinarch.di.modules


import dagger.Module


/**
 * All your Activities participating in DI would be listed here.
 */
@Module(includes = [FragmentModule::class]) // Including Fragment Module Available For Activities
abstract class ActivityModule {

    /**
     * Marking Activities to be available to contributes for Android Injector
     */
//    @ContributesAndroidInjector
//    abstract fun contributeOnBoardingActivity(): OnBoardingActivity


}
