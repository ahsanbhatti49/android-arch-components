package com.bhatti.mvvm.kotlinarch.di.modules


import dagger.Module

@Suppress("unused")
@Module
abstract class FragmentModule {

    /**
     * Injecting Fragments
     */
//    @ContributesAndroidInjector
//    internal abstract fun contributeMobileNumberAndOtpFagment(): MobileNumberAndOtpFagment

}