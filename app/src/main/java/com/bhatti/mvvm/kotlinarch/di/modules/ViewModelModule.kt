package com.bhatti.mvvm.kotlinarch.di.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.bhatti.mvvm.kotlinarch.di.base.ViewModelFactory
import com.bhatti.mvvm.kotlinarch.di.base.ViewModelKey
import com.bhatti.mvvm.kotlinarch.repository.model.SharedViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {

    /**
     * Binds ViewModels factory to provide ViewModels.
     */
    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(SharedViewModel::class)
    abstract fun bindSharedViewModel(sharedData: SharedViewModel): ViewModel

}
