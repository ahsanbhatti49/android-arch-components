package com.bhatti.mvvm.kotlinarch.repository.api

import androidx.lifecycle.LiveData
import com.bhatti.mvvm.kotlinarch.repository.model.NetworkResponse
import com.bhatti.mvvm.kotlinarch.repository.api.network.Resource
import retrofit2.http.*


/**
 * Created by ahsan on 04,December,2019
 */

/**
 * Api services to communicate with server
 *
 */
interface ApiServices {
    @FormUrlEncoded
    @Headers("Accept: application/json")
    @POST("{path}")
    fun getDataFromNetwork(
        @HeaderMap header: HashMap<String,String>,
        @Path(value = "path", encoded = true) path: String,
        @FieldMap params: HashMap<String, String>
    ): LiveData<Resource<NetworkResponse>>


}
