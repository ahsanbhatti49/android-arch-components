package com.bhatti.mvvm.kotlinarch.repository.api.network

import android.content.Context
import androidx.annotation.MainThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import com.bhatti.mvvm.kotlinarch.repository.model.NetworkResponse
import com.bhatti.mvvm.kotlinarch.utils.extensions.putString
import com.finja.business.utils.AppConstants


/**
 * A generic class to send loading event up-stream when fetching data
 * only from network.
 *
 */
abstract class NetworkResource<RequestType> @MainThread constructor(private val context: Context) {

    /**
     * The final result LiveData
     * MediatorLiveData is a LiveData subclass which may observe
     * other LiveData objects and react on OnChanged events from them.
     *
     * This class correctly propagates its active/inactive states down to source LiveData objects.
     */
    private val result = MediatorLiveData<Resource<RequestType>>()

    init {
        // Send loading state to UI
        result.value = Resource.loading()
        fetchFromNetwork()
    }

    /**
     * Fetch the data from network and then send it upstream to UI.
     */
    private fun fetchFromNetwork() {
        val apiResponse = createCall()
        result.addSource(apiResponse) { response ->

            result.removeSource(apiResponse)
            // Dispatch the result
            response?.apply {
                when {
                    status.isSuccessful() -> {
                        val respnse: NetworkResponse = data as NetworkResponse

                        respnse.accessToken?.let {
                            AppConstants.ACCESS_TOKEN_KEY.putString(it).subscribe()
                        }
                        when (respnse.code) {
                            "200" -> setValue(this)
                            else -> {
                                setValue(Resource.error(respnse.message))
                            }
                        }
                    }
                    else -> {
                        setValue(Resource.error(errorMessage))
                    }
                }
            }
        }
    }

    fun asLiveData(): LiveData<Resource<RequestType>> {
        return result
    }

    @MainThread
    private fun setValue(newValue: Resource<RequestType>) {
        if (result.value != newValue)
            result.value = newValue
    }

    @MainThread
    protected abstract fun createCall(): LiveData<Resource<RequestType>>
}