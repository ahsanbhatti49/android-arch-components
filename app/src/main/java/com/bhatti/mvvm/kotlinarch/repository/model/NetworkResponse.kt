package com.bhatti.mvvm.kotlinarch.repository.model

import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

//@Entity(tableName = "home_menu_table")
data class NetworkResponse(

    @PrimaryKey(autoGenerate = true) var id: Int = 0,
    @SerializedName("code")
    @Expose
    var code: String? = null,
    @SerializedName("message")
    @Expose
    var message: String? = null,
    @SerializedName("accessToken")
    @Expose
    var accessToken: String? = null,
    @SerializedName("data")
    @Expose
    var data: LinkedHashMap<String, Any>? = null

)