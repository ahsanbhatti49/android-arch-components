package com.bhatti.mvvm.kotlinarch.repository.model

import android.os.Bundle
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.bhatti.mvvm.kotlinarch.repository.repo.CommonRepository
import javax.inject.Inject

class SharedViewModel @Inject constructor(
    private val commonRepository: CommonRepository
) : ViewModel() {

    val data = MutableLiveData<Any>()

    fun setData(data: Bundle) {
        this.data.value = data
    }
}