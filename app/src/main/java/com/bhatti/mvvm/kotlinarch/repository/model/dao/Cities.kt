package com.bhatti.mvvm.kotlinarch.repository.model.dao

import com.bhatti.mvvm.kotlinarch.ui.adapter.ListItemView
import com.google.gson.annotations.SerializedName


data class Cities(
    @SerializedName("id") var cityId: String? = null,
    @SerializedName("name") var cityTitle: String? = null,
    var isChecked: Int=0
    ) : ListItemView()

