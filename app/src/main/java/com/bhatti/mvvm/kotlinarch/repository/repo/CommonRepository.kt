package com.bhatti.mvvm.kotlinarch.repository.repo

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.provider.Settings
import androidx.lifecycle.LiveData
import com.bhatti.mvvm.kotlinarch.BuildConfig
import com.finja.business.app.AppExecutors
import com.bhatti.mvvm.kotlinarch.repository.api.ApiServices
import com.bhatti.mvvm.kotlinarch.repository.api.network.NetworkResource
import com.bhatti.mvvm.kotlinarch.repository.api.network.Resource
import com.bhatti.mvvm.kotlinarch.repository.model.NetworkResponse
import com.bhatti.mvvm.kotlinarch.utils.extensions.getString
import com.bhatti.mvvm.kotlinarch.utils.locale.LanguageUtil
import com.finja.business.utils.AppConstants
import com.finja.business.utils.Keys
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by ahsan on 29,November,2019
 */

@Singleton
class CommonRepository @Inject constructor(
    private val apiServices: ApiServices, private val context: Context,
    private val appExecutors: AppExecutors = AppExecutors()
) {


    /**
     * Fetch the Data  from web
     * LiveData<Resource<NetworkResponse>>
     */
    @SuppressLint("CheckResult")
    fun getDataFromNetwork(apiId: Int, param: HashMap<String, String>):
            LiveData<Resource<NetworkResponse>> {

        val credentials = Keys.apiKey(apiId)
        val header = HashMap<String, String>()
        if (credentials.length >= 2) {
            credentials.split(",")[0]
            header["appId"] = credentials.split(",")[1]
            header["appKey"] = credentials.split(",")[2]
            AppConstants.ACCESS_TOKEN_KEY.getString("0")
                .subscribe { header[AppConstants.ACCESS_TOKEN_KEY] = it }
        }

        getDefaultParams(param)

        return object : NetworkResource<NetworkResponse>(context) {
            override fun createCall(): LiveData<Resource<NetworkResponse>> {
                return apiServices.getDataFromNetwork(header, credentials.split(",")[0], param)
            }

        }.asLiveData()


    }

    @SuppressLint("CheckResult", "HardwareIds")
    private fun getDefaultParams(param: HashMap<String, String>): HashMap<String, String> {
        param["deviceId"] =
            Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
        param["origin"] = BuildConfig.ORIGIN
        param["deviceDetail"] = Build.MODEL
        return param
    }

}