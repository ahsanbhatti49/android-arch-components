package com.bhatti.mvvm.kotlinarch.ui

import android.app.Activity
import android.content.Context
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.bhatti.mvvm.kotlinarch.repository.model.SharedViewModel
import com.bhatti.mvvm.kotlinarch.utils.extensions.getViewModel
import com.bhatti.mvvm.kotlinarch.utils.locale.LanguageUtil
import com.bhatti.mvvm.kotlinarch.utils.locale.LocaleHelper
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import dagger.android.DispatchingAndroidInjector
import dagger.android.support.HasSupportFragmentInjector
import javax.inject.Inject


/**
 * Created by ahsan on 02,December,2019
 */

typealias BaseActivity = DaggerActivity

/**
 * Activity providing Dagger support and [ViewModel] support
 */
abstract class DaggerActivity : AppCompatActivity(), HasSupportFragmentInjector {

    @Inject
    lateinit var dispatchingAndroidInjector: DispatchingAndroidInjector<Fragment>

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun supportFragmentInjector() = dispatchingAndroidInjector

    var language = LanguageUtil.LANGUAGE_EN


    private val tvToolbarTitle: TextView? = null
    private val toolbar: Toolbar? = null
    private var finjaImage: ImageView? = null

    private val sharedViewModel by lazy {
        getViewModel<SharedViewModel>()
    }


    /**
     * Extension function to make changes in FragmentTransaction
     */
    inline fun FragmentManager.inTransaction(func: FragmentTransaction.() -> FragmentTransaction) {
        beginTransaction().func().commit()
    }

    /**
     * Extension function to make changes in FragmentTransaction
     */
    inline fun FragmentManager.transaction(func: FragmentTransaction.() -> FragmentTransaction) {
        beginTransaction().func().commit()
    }

    inline fun <reified T> fromJson(json: String): T {
        return Gson().fromJson(json, object : TypeToken<T>() {}.type)
    }


    fun add(fragment: Fragment, container: Int) {
        supportFragmentManager.transaction {
            add(container, fragment)
        }
    }

    fun popFragment(count: Int) {
        for (item in 0 until count) {
            supportFragmentManager.popBackStack()
        }
    }

    fun popFragmentAll() {
        for (item in 0 until supportFragmentManager.backStackEntryCount) {
            supportFragmentManager.popBackStack()
        }
    }

    fun fragmentCount(): Int {
        return supportFragmentManager.backStackEntryCount
    }

    fun replace(fragment: Fragment, container: Int) {
        supportFragmentManager.transaction {
            replace(container, fragment)
        }
    }

    /**
     *
     */
    fun replaceFragment(
        fragment: Fragment, frameId: Int, addToStack: Boolean, clearBackStack: Boolean
    ) {
        supportFragmentManager.inTransaction {

            if (clearBackStack && supportFragmentManager.backStackEntryCount > 0) {
                val first = supportFragmentManager.getBackStackEntryAt(0)
                supportFragmentManager.popBackStack(
                    first.id,
                    FragmentManager.POP_BACK_STACK_INCLUSIVE
                )
            }

            if (addToStack) replace(frameId, fragment, fragment.javaClass.simpleName)
                .addToBackStack(fragment.javaClass.simpleName)
            else
                replace(frameId, fragment, fragment.javaClass.simpleName)
        }
    }

    fun addFragment(fragment: Fragment, frameId: Int, addToStack: Boolean) {
        supportFragmentManager.inTransaction {
            if (addToStack) add(frameId, fragment, "")
                .addToBackStack(fragment.javaClass.simpleName)
            else add(frameId, fragment)
        }
    }

    fun addFragment(fragment: Fragment, frameId: Int, addToStack: Boolean, tag: String) {
        supportFragmentManager.inTransaction {
            if (addToStack) add(frameId, fragment, tag)
                .addToBackStack(fragment.javaClass.simpleName)
            else add(frameId, fragment)
        }
    }



    fun initSharedViewModel() {
        sharedViewModel.data.observe(this, Observer {
            it.let {

            }
        })
    }

    fun setFinjaTitle(imageView: ImageView) {
        finjaImage = imageView
    }


    /**
     *
     */
    open fun hideKeyboard() {
        val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        var view = currentFocus
        if (view == null) {
            view = View(this)
        }
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }



    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(LocaleHelper.onAttach(newBase!!, LanguageUtil.LANGUAGE_EN))
//        super.attachBaseContext(MyContextWrapper.wrap(newBase, language))
    }
}
