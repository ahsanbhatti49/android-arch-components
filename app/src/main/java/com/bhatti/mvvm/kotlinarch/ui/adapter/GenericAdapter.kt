package com.bhatti.mvvm.kotlinarch.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.databinding.library.baseAdapters.BR
import androidx.recyclerview.widget.RecyclerView
import hu.tamaskozmer.bindableadapterexample.BindableAdapter

class GenericAdapter<T : ListItemView>(@LayoutRes val layoutId: Int) :
    RecyclerView.Adapter<GenericAdapter.GenericViewHolder<T>>(), BindableAdapter<T> {

    private val items = mutableListOf<T>()
    private var inflater: LayoutInflater? = null
    private var onListItemViewClickListener: OnListItemViewClickListener? = null
    private var onListSubItemViewClickListener: OnListSubItemViewClickListener? = null

    fun addItems(items: List<T>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    fun setOnListItemViewClickListener(onListItemViewClickListener: OnListItemViewClickListener?) {
        this.onListItemViewClickListener = onListItemViewClickListener
    }

    fun setOnListSubItemViewClickListener(onListSubItemViewClickListener: OnListSubItemViewClickListener?) {
        this.onListSubItemViewClickListener = onListSubItemViewClickListener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GenericViewHolder<T> {
        val layoutInflater = inflater ?: LayoutInflater.from(parent.context)
        val binding =
            DataBindingUtil.inflate<ViewDataBinding>(layoutInflater, layoutId, parent, false)
        return GenericViewHolder(binding)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: GenericViewHolder<T>, position: Int) {
        val itemViewModel = items[position]
        itemViewModel.adapterPosition = position
        onListItemViewClickListener?.let { itemViewModel.onListItemViewClickListener = it }
        onListSubItemViewClickListener?.let { itemViewModel.onListSubItemViewClickListener = it }
        holder.bind(itemViewModel)
    }


    class GenericViewHolder<T : ListItemView>(private val binding: ViewDataBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(itemView: T) {
            binding.setVariable(BR.item, itemView)
            binding.executePendingBindings()
        }
    }

    interface OnListItemViewClickListener {
        fun onClick(view: View, position: Int)
    }

    interface OnListSubItemViewClickListener {
        fun onClick(view: View, position: Int)
    }

    override fun setData(items: List<T>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }
}
