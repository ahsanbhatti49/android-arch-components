package com.bhatti.mvvm.kotlinarch.ui.adapter

abstract class ListItemView{
    var adapterPosition: Int = -1
    var onListItemViewClickListener: GenericAdapter.OnListItemViewClickListener? = null
    var onListSubItemViewClickListener: GenericAdapter.OnListSubItemViewClickListener? = null
}