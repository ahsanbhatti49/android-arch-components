package com.finja.business.utils

import android.content.Context
import javax.inject.Inject
import javax.inject.Singleton

/**
 * all constant and static values must be stored here
 */
@Singleton
class AppConstants @Inject constructor(
    private val context: Context
) {

    companion object {

        var languageCall = 0
        var ACCESS_TOKEN_KEY = "acceessToken"

    }
}