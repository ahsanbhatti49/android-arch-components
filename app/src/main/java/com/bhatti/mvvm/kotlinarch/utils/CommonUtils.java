package com.bhatti.mvvm.kotlinarch.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.AssetManager;
import android.provider.Settings;
import android.view.View;

import java.io.IOException;
import java.io.InputStream;


public final class CommonUtils {

    private CommonUtils() {
        // This utility class is not publicly instantiable
    }

    @SuppressLint("all")
    public static String getDeviceId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(), Settings.Secure.ANDROID_ID);
    }

    public static String loadJSONFromAsset(Context context, String jsonFileName) throws IOException {
        AssetManager manager = context.getAssets();
        InputStream inputStream = manager.open(jsonFileName);
        int size = inputStream.available();
        byte[] buffer = new byte[size];
        inputStream.read(buffer);
        inputStream.close();
        return new String(buffer, "UTF-8");
    }

        public static String capitalise(String name) {
            String collect[] = name.split(" ");
            String returnName = "";
            for (int i = 0; i < collect.length; i++) {
                collect[i] = collect[i].trim().toLowerCase();
                if (collect[i].isEmpty() == false) {
                    returnName = returnName + collect[i].substring(0, 1).toUpperCase() + collect[i].substring(1) + " ";
                }
            }
            return returnName.trim();
        }
    public static void preventTwoClick(final View view){
        view.setEnabled(false);
        view.postDelayed(() -> view.setEnabled(true), 500);
    }

}