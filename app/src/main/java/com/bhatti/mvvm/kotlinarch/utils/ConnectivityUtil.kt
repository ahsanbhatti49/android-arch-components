package com.finja.business.utils

import android.content.Context
import android.net.ConnectivityManager
import com.bhatti.mvvm.kotlinarch.utils.NotificationUtil

/**
 * Created by Waheed on 04,November,2019
 */

object ConnectivityUtil {

    fun isConnected(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkInfo = connectivityManager.activeNetworkInfo
        return if (networkInfo != null && networkInfo.isConnected) {
            true
        } else {
            NotificationUtil.showCustomToast(context, "No Internet Connection")
            false
        }
//        return networkInfo != null && networkInfo.isConnected
    }
}