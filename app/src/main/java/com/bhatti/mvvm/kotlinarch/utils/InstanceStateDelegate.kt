package com.finja.business.utils

interface InstanceStateDelegate {
    fun <T> instanceState(): InstanceStateProvider.Nullable<T>
    fun <T> instanceState(defaultValue: T): InstanceStateProvider.NotNull<T>
//    fun <T> lateinitInstanceState(): InstanceStateProvider.Lateinit<T>
}