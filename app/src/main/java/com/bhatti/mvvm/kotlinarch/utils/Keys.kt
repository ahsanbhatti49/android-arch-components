package com.finja.business.utils

object Keys {
    init {
        System.loadLibrary("native-lib")
    }

    external fun apiKey(apiID: Int): String
}