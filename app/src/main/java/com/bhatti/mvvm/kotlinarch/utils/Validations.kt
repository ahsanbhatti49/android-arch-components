package com.bhatti.mvvm.kotlinarch.utils

import android.content.Context
import android.graphics.Typeface
import android.text.Spannable
import android.text.SpannableString
import android.widget.EditText
import com.finja.business.utils.CustomTypefaceSpan
import com.finja.business.utils.customViews.MyDrawableEditText
import com.finja.business.utils.customViews.MyEditText
import com.google.android.material.textfield.TextInputLayout
import java.util.regex.Pattern
import javax.inject.Inject
import javax.inject.Singleton

@Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")
@Singleton
class Validations @Inject constructor(
    context: Context
) {
    private final val VALID_EMAIL_ADDRESS_REGEX = Pattern.compile(
        "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
        Pattern.CASE_INSENSITIVE
    )

    val typeface =
        Typeface.createFromAsset(context.assets, "fonts/Poppins-Medium.ttf")

    /**
     * @param editTexts
     * @return
     */
    fun isValid(vararg editTexts: MyEditText): Boolean {
        val valid = true
        for (editText in editTexts) {
            val parent = editText.parent.parent as TextInputLayout
            parent.error = null
            editText.error = null
            if (getStrings(editText)!!.isEmpty()) {
                val s = SpannableString("Field Required")
                s.setSpan(
                    CustomTypefaceSpan(typeface),
                    0,
                    s.length,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                )
                parent.error = s
                editText.requestFocus()
                return false
            }
        }
        return true
    }
    fun isValid(vararg editTexts: MyDrawableEditText): Boolean {
        val valid = true
        for (editText in editTexts) {
            val parent = editText.parent.parent as TextInputLayout
            parent.error = null
            editText.error = null
            if (getStrings(editText)!!.isEmpty()) {
                val s = SpannableString("Field Required")
                s.setSpan(
                    CustomTypefaceSpan(typeface),
                    0,
                    s.length,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                )
                parent.error = s
                editText.requestFocus()
                return false
            }
        }
        return true
    }

    /**
     * @param editText
     * @return
     */
    fun isValidEmail(editText: MyEditText): Boolean {
        val valid = true
        val parent =
            editText.parent.parent as TextInputLayout
        parent.error = null
        if (getString(editText)!!.isEmpty()) {
            val s = SpannableString("Email Required")
            s.setSpan(
                CustomTypefaceSpan(typeface),
                0,
                s.length,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            parent.error = s
            editText.requestFocus()
            return false
        } else {
            val matcher =
                VALID_EMAIL_ADDRESS_REGEX.matcher(getString(editText))
            if (!matcher.find()) {
                val s = SpannableString("Invalid Email")
                s.setSpan(
                    CustomTypefaceSpan(typeface),
                    0,
                    s.length,
                    Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
                )
                parent.error = s
                editText.requestFocus()
                return false
            }
        }
        return true
    }

    /**
     * @param editText
     * @return
     */
    private fun getStrings(editText: EditText): String? {
        return editText.text.toString().trim { it <= ' ' }
    }

    /**
     * @param editText
     * @return
     */
    fun getString(editText: MyEditText): String? {
        return editText.text.toString().trim { it <= ' ' }
    }




}