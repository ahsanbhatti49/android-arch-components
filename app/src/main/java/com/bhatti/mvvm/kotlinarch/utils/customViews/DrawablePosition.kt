package com.finja.business.utils.customViews

enum class DrawablePosition {
    LEFT,
    RIGHT,
    TOP,
    BOTTOM
}