package com.finja.business.utils.customViews

import android.content.Context
import android.graphics.Typeface
import android.util.AttributeSet
import android.widget.Button
import com.bhatti.mvvm.kotlinarch.R

class MyButton :
    Button {
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init(attrs)
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        init(attrs)
    }

    constructor(context: Context?) : super(context)

    private fun init(attrs: AttributeSet?) {
        if (attrs != null) {
            val a = context.obtainStyledAttributes(attrs, R.styleable.MyButtonView)
            val fontName = a.getString(R.styleable.MyButtonView_fontName)
            if (fontName != null) {
                val myTypeface =
                    Typeface.createFromAsset(context.assets, "fonts/$fontName")
                typeface = myTypeface
            } else
                Typeface.createFromAsset(context.assets, "fonts/Poppins-Light.ttf")
            a.recycle()
            isAllCaps = false
        }
    }


}