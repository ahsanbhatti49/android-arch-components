package com.bhatti.mvvm.kotlinarch.utils.customViews;

import android.content.Context;
import android.graphics.Typeface;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.EditText;


import com.bhatti.mvvm.kotlinarch.R;
import com.google.android.material.textfield.TextInputLayout;

import java.lang.reflect.Field;

/**
 * Created by ahsan on 02/01/2019.
 */

public class MyCustomInputLayout extends TextInputLayout {

    //    public MyCustomInputLayout(Context context) {
//        super(context);
//
//    }


    @Override
    public void setErrorTextAppearance(int resId) {
        super.setErrorTextAppearance(resId);
    }

    public MyCustomInputLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
        initFont(context);
//        setErrorEnabled(true);
        setErrorTextAppearance(R.style.errorAppearance);

    }

    private void initFont(Context context) {

        EditText editText = getEditText();

        if (editText != null) {
            Typeface typeface = Typeface.createFromAsset(
                    getContext().getAssets(), "fonts/futura_light_bt.ttf");
            editText.setTypeface(typeface);
            setErrorEnabled(true);

            try {
                // Retrieve the CollapsingTextHelper Field
                final Field cthf = TextInputLayout.class.getDeclaredField("mCollapsingTextHelper");
                cthf.setAccessible(true);


                // Retrieve an instance of CollapsingTextHelper and its TextPaint
                final Object cth = cthf.get(this);
                final Field tpf = cth.getClass().getDeclaredField("mTextPaint");
                tpf.setAccessible(true);

                // Apply your Typeface to the CollapsingTextHelper TextPaint
                ((TextPaint) tpf.get(cth)).setTypeface(typeface);

            } catch (Exception ignored) {
                // Nothing to do
                Log.d("sdf", ignored.getMessage());
            }
        }

//        this.sty

    }


}
