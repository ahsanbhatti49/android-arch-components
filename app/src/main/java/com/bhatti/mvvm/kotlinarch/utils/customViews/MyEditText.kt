package com.finja.business.utils.customViews

import android.content.Context
import android.content.res.TypedArray
import android.graphics.Typeface
import android.util.AttributeSet
import android.widget.EditText
import com.bhatti.mvvm.kotlinarch.R

class MyEditText :
    EditText {
    constructor(context: Context?, attrs: AttributeSet?, defStyleAttr: Int) : super(
        context,
        attrs,
        defStyleAttr
    ) {
        init(attrs)
    }

    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context?) : super(context)


    private fun init(attrs: AttributeSet?) {
        if (attrs != null) {
            val a: TypedArray = context!!.obtainStyledAttributes(attrs, R.styleable.MyTextView)
            val fontName = a.getString(R.styleable.MyTextView_fontName)
            if (fontName != null) {
                val myTypeface =
                    Typeface.createFromAsset(context.assets, "fonts/$fontName")
                typeface = myTypeface
            } else
                Typeface.createFromAsset(context.assets, "fonts/Poppins-Light.ttf")
            a.recycle()
            maxLines = 1
            isLongClickable = false
            textSize = 14.0f

        }
    }
}