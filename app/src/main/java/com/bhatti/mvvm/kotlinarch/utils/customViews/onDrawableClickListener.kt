package com.finja.business.utils.customViews


interface OnDrawableClickListener {
    fun onClick(target: DrawablePosition)
}