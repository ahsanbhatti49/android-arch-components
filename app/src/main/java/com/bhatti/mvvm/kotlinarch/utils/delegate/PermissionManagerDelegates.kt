package com.bhatti.mvvm.kotlinarch.utils.delegate

interface PermissionManagerDelegate {
    fun requestAndRun(permissions: List<String>, failAction: () -> Unit, action: () -> Unit)
    fun requestThenRun(permissions: List<String>, failAction: () -> Unit, action: () -> Unit)
}
