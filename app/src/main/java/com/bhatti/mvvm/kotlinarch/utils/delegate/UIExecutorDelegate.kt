package com.bhatti.mvvm.kotlinarch.utils.delegate

interface UIExecutorDelegate {
    fun runOnUi(command: Runnable)
    fun postOnUi(command: Runnable)
    fun delayOnUi(afterMs: Long, command: Runnable)

    fun runOnUi(command: () -> Unit)
    fun postOnUi(command: () -> Unit)
    fun delayOnUi(afterMs: Long, command: () -> Unit)
}