package com.bhatti.mvvm.kotlinarch.utils.extensions

import android.content.Context
import android.content.SharedPreferences

object FinjaPreferences {
    var sharedPreferences: SharedPreferences? = null
    fun init(context: Context, preferencesName: String = context.packageName) {
        sharedPreferences = context.getSharedPreferences(preferencesName, Context.MODE_PRIVATE)
    }
}
