package com.bhatti.mvvm.kotlinarch.utils.extensions

import io.reactivex.Observable

fun String.getBoolean(defaultValue: Boolean = false) : Observable<Boolean> {
    FinjaPreferences.sharedPreferences.let { sharedPreferences ->
        return Observable.create {
            sharedPreferences?.getBoolean(this, defaultValue)?.let { it1 -> it.onNext(it1) }
            it.onComplete()
        }
    }
}

fun String.getInt(defaultValue: Int = 0) : Observable<Int> {
    FinjaPreferences.sharedPreferences.let { sharedPreferences ->
        return Observable.create {
            sharedPreferences?.getInt(this, defaultValue)?.let { it1 -> it.onNext(it1) }
            it.onComplete()
        }
    }
}

fun String.getFloat(defaultValue: Float = 0f) : Observable<Float> {
    FinjaPreferences.sharedPreferences.let { sharedPreferences ->
        return Observable.create {
            sharedPreferences?.getFloat(this, defaultValue)?.let { it1 -> it.onNext(it1) }
            it.onComplete()
        }
    }
}

fun String.getLong(defaultValue: Long = 0L) : Observable<Long> {
    FinjaPreferences.sharedPreferences.let { sharedPreferences ->
        return Observable.create {
            sharedPreferences?.getLong(this, defaultValue)?.let { it1 -> it.onNext(it1) }
            it.onComplete()
        }
    }
}

fun String.getString(defaultValue: String) : Observable<String> {
    FinjaPreferences.sharedPreferences.let { sharedPreferences ->
        return Observable.create {
            sharedPreferences?.getString(this, defaultValue)?.let { it1 -> it.onNext(it1) }
            it.onComplete()
        }
    }
}

fun String.getStringSet(defaultValue: Set<String>) : Observable<Set<String>> {
    FinjaPreferences.sharedPreferences.let { sharedPreferences ->
        return Observable.create {
            sharedPreferences?.getStringSet(this, defaultValue)?.let { it1 -> it.onNext(it1) }
            it.onComplete()
        }
    }
}

fun String.putString(value: String) : Observable<String> {
    FinjaPreferences.sharedPreferences.let { sharedPreferences ->
        return Observable.just(value)
            .doOnNext {
                sharedPreferences?.edit()?.putString(this, value)?.apply()
            }
    }
}

fun String.putInt(value: Int) : Observable<Int> {
    FinjaPreferences.sharedPreferences.let { sharedPreferences ->
        return Observable.just(value)
            .doOnNext {
                sharedPreferences?.edit()?.putInt(this, value)?.apply()
            }
    }
}

fun String.putLong(value: Long) : Observable<Long> {
    FinjaPreferences.sharedPreferences.let { sharedPreferences ->
        return Observable.just(value)
            .doOnNext {
                sharedPreferences?.edit()?.putLong(this, value)?.apply()
            }
    }
}

fun String.putBoolean(value: Boolean) : Observable<Boolean> {
    FinjaPreferences.sharedPreferences.let { sharedPreferences ->
        return Observable.just(value)
            .doOnNext {
                sharedPreferences?.edit()?.putBoolean(this, value)?.apply()
            }
    }
}

fun String.putFloat(value: Float) : Observable<Float> {
    FinjaPreferences.sharedPreferences.let { sharedPreferences ->
        return Observable.just(value)
            .doOnNext {
                sharedPreferences?.edit()?.putFloat(this, value)?.apply()
            }
    }
}

fun String.putStringSet(value: Set<String>) : Observable<Set<String>> {
    FinjaPreferences.sharedPreferences.let { sharedPreferences ->
        return Observable.just(value)
            .doOnNext {
                sharedPreferences?.edit()?.putStringSet(this, value)?.apply()
            }
    }
}