package com.bhatti.mvvm.kotlinarch.utils.locale

import android.annotation.SuppressLint
import com.bhatti.mvvm.kotlinarch.utils.extensions.getString
import com.bhatti.mvvm.kotlinarch.utils.extensions.putString

object LanguageUtil {

    // constants for language
    const val LANGUAGE_EN: String = "en"
    const val LANGUAGE_UR: String = "ur"
    const val SELECTED_LANGUAGE: String="SELECTED_LANGUAGE"

    fun setLanguageCode(language: String) {
        when(language){
            "English" ->{
                language.let {
                    SELECTED_LANGUAGE.putString(LANGUAGE_EN).subscribe()
                }
            }
            "Urdu" -> {
                language.let {
                    SELECTED_LANGUAGE.putString(LANGUAGE_UR).subscribe()
                }
            }
        }

    }

    @SuppressLint("CheckResult")
    fun getLanguageCode() :String{
      var value = ""
        SELECTED_LANGUAGE.getString(LANGUAGE_EN).subscribe{
            value=it
        }
        return value
    }

    @SuppressLint("CheckResult")
    fun getLanguageId() :String{
        var value = ""
        SELECTED_LANGUAGE.getString(LANGUAGE_EN).subscribe{
            value=it
        }

        value = when(value){
            LANGUAGE_EN ->{ "1"  }
            LANGUAGE_UR -> { "2" }
            else -> "1"
        }

        return value
    }
}