package com.bhatti.mvvm.kotlinarch.utils.locale

import android.annotation.SuppressLint
import android.content.Context
import com.bhatti.mvvm.kotlinarch.utils.extensions.getString
import com.bhatti.mvvm.kotlinarch.utils.extensions.putString
import java.util.*


class LocaleHelper {

    companion object {
        private fun onAttach(context: Context): Context {
            var defaultLanguage = getPersistedData(Locale.getDefault().language)
            return setLocale(context, defaultLanguage!!)
        }

        fun onAttach(context: Context, defaultLanguage: String): Context {
            var language = getPersistedData(defaultLanguage)
            return setLocale(context, language!!)
        }

        fun setLocale(context: Context, language: String): Context {
            persist(language)
            return updateResources(context, language)
        }

        private fun updateResources(context: Context, language: String): Context {
            var locale = Locale(language)
            Locale.setDefault(locale)

            var configuration = context.resources.configuration
            configuration.setLocale(locale)
            configuration.setLayoutDirection(locale)

            return context.createConfigurationContext(configuration)
        }

        private fun persist(language: String) {
            language?.let {
                LanguageUtil.SELECTED_LANGUAGE.putString(it).subscribe()
            }
        }

        @SuppressLint("CheckResult")
        private fun getPersistedData(language: String?): String? {
            var value = ""
            LanguageUtil.SELECTED_LANGUAGE.getString(language!!).subscribe {
                value = it
            }
            return value
        }
    }

}