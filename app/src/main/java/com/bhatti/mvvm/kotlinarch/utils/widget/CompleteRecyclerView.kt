package com.bhatti.mvvm.kotlinarch.utils.widget

import android.content.Context
import android.util.AttributeSet
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bhatti.mvvm.kotlinarch.utils.extensions.gone
import com.bhatti.mvvm.kotlinarch.utils.extensions.visible
import com.bhatti.mvvm.kotlinarch.repository.api.network.Status
import com.finja.business.utils.customViews.MyTextView

/**
 * A custom implementation of [RecyclerView] to support
 * Empty View & Loading animation.
 */
class CompleteRecyclerView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyle: Int = 0
) : RecyclerView(context, attrs, defStyle) {

    /**
     * Empty layout
     */

    private var emptyImage: ImageView? = null
    private var emptyText: MyTextView? = null

    /**
     * Progress view
     */

    private var progressBar: ProgressBar? = null
    private var progressTextView: MyTextView? = null

    /**
     * Column width for grid layout
     */
    private var columnWidth: Int = 0

    init {
        gone()
        if (attrs != null) {
            val attrsArray = intArrayOf(android.R.attr.columnWidth)
            val array = context.obtainStyledAttributes(
                attrs, attrsArray
            )
            columnWidth = array.getDimensionPixelSize(0, -1)
            array.recycle()
        }
    }

    override fun setAdapter(adapter: RecyclerView.Adapter<*>?) {
        visible()
        val oldAdapter = getAdapter()
        oldAdapter?.unregisterAdapterDataObserver(mAdapterObserver)
        super.setAdapter(adapter)
        adapter?.registerAdapterDataObserver(mAdapterObserver)
        refreshState()
    }

    private fun refreshState() {
        adapter?.let {
            val noItems = 0 == it.itemCount
            if (noItems) {

                this.progressBar?.gone()
                this.progressTextView?.gone()

                emptyText?.visible()
                emptyImage?.visible()

                gone()
            } else {

                emptyText?.gone()
                emptyImage?.gone()

                progressBar?.gone()
                progressTextView?.gone()

                visible()
            }
        }
    }


    fun setEmptyView(imageView: ImageView, textView: MyTextView? = null) {
        emptyImage = imageView
        emptyText = textView
        emptyText?.gone()
        emptyImage?.gone()

    }

    fun setProgressView(progress: ProgressBar, textView: MyTextView? = null) {
        this.progressBar = progress
        this.progressTextView = textView
        this.progressBar?.gone()
        this.progressTextView?.gone()

    }


    fun setEmptyMessage(@StringRes mEmptyMessageResId: Int) {
        this.emptyText?.setText(mEmptyMessageResId)
    }

    fun setEmptyIcon(@DrawableRes mEmptyIconResId: Int) {
        emptyImage?.setImageResource(mEmptyIconResId)
    }

    fun showState(status: Status) {
        when (status) {
            Status.SUCCESS, Status.ERROR -> {

                this.progressBar?.gone()
                this.progressTextView?.gone()

                emptyText?.visible()
                emptyImage?.visible()


            }
            Status.LOADING -> {

                emptyText?.gone()
                emptyImage?.gone()

                progressBar?.visible()
                progressTextView?.visible()


            }
        }
    }

    override fun onMeasure(widthSpec: Int, heightSpec: Int) {
        super.onMeasure(widthSpec, heightSpec)
        if (layoutManager is GridLayoutManager) {
            val manager = layoutManager as GridLayoutManager
            if (columnWidth > 0) {
                val spanCount = Math.max(1, measuredWidth / columnWidth)
                manager.spanCount = spanCount
            }
        }
    }

    /**
     * Observes for changes in the adapter and is triggered on change
     */
    private val mAdapterObserver = object : RecyclerView.AdapterDataObserver() {
        override fun onChanged() = refreshState()
        override fun onItemRangeInserted(positionStart: Int, itemCount: Int) = refreshState()
        override fun onItemRangeRemoved(positionStart: Int, itemCount: Int) = refreshState()
    }

}
