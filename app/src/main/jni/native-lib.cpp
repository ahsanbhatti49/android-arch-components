//
// Created by Ahsan on 2019-12-18.
//

#include <jni.h>
#include <string>

extern "C" JNIEXPORT jstring

JNICALL
Java_com_finja_business_utils_Keys_apiKey(JNIEnv *env, jobject object, jint apiId) {
    int api_id = (int) apiId;

    switch (api_id) {
        case 1:
            return env->NewStringUTF("end_point,key1,key2");

        default:
            return env->NewStringUTF("");
    }
}




